module Game
where 

import UI
import GenToken
import Data.List
import System.Exit
import System.IO


initBoard :: Int -> Board
initBoard s = [[Blank | _ <- [1..s]] | _ <- [1..s]]


-- moves are 'w', 'a', 's', 'd'


	

transformBoard :: Board -> Int -> Char -> Int -> IO ()
transformBoard board score move isHard = 
				if move == 'x'
						then do
							putStrLn "Exiting the game"
							putStr "Your Score :"
							putStrLn (show score)
						else do	
							let (b, s) = applyMoves board 0 move
							-- putStr "Completed move\n"
							playGame b (score + s) ( (b == board) && (s == 0) ) isHard


playGame :: Board -> Int -> Bool -> Int -> IO ()
playGame board score isSame isHard = do
	board <- (if isSame 
					then return board
					else if (board == initBoard (length board)) then  (genToken board score 0) else (genToken board score isHard))
	if  (isWin board (numberToWin (length board)) )
						then do
							putStr "\t\tYou win :D\n\n"
							putStr "\t\tScore: "
							putStr (show score)
							putStr "\n\n\t\tGame Over\n\n"
						else do
							if (isLose board)
											then do
												putStr "\t\tYou lose :( \n\n"
												putStr "\t\tScore: "
												putStr (show score)
												putStr "\n\n\t\tGame Over\n\n"
											else do
												prettyPrint board score
												hFlush stdout
												move <- (getInput "wasdx")
												putChar move
												transformBoard board score move isHard


getInput :: String -> IO Char
getInput cs = do
	move <- getChar
	if move `elem` cs then return move else (getInput cs)

numberToWin :: Int -> Int
numberToWin sz 
			| sz == 1     =  4
			| sz == 2	  =  32
			| sz == 3	  =  1024
			| sz == 4	  =	 2048
			| sz <= 6	  =	 4096
			| otherwise   =  8192

isWin :: Board -> Int -> Bool
isWin board  num 	= 	foldr ( (||).(foldr ((||).(checkIf num)) False) ) False board

checkIf :: Int -> Cell Int -> Bool 
checkIf _ Blank 	= False
checkIf num (V x)	= (x == num)

isLose :: Board -> Bool
isLose board = foldr ((&&).(isUnchanged board)) True "wasdx"

isUnchanged :: Board -> Char -> Bool
isUnchanged board x = 
	let (b, s) = applyMoves board 0 x
	in (if ( (board == b) && (s == 0) ) then True else False)