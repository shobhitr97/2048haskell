module GenToken
where 

import UI
import System.Random
import Data.List
import System.Exit
import System.IO

-- shiftBoard will move the cells to the left, i.e., it will like the player chose the left swipe
shiftBoard :: Board -> (Board, Int)
shiftBoard []	  = ([], 0)
shiftBoard (r:rs) = let {
							(r', s') 	= shiftRow r [] [] 0 (length r);
							(b, s)		= shiftBoard rs
						}
					in 	((r':b), s + s')


-- a helper function to shiftBoard
shiftRow :: [Cell Int] -> [Cell Int] -> [Cell Int] -> Int -> Int -> ([Cell Int], Int)
shiftRow [] _ r' s	sz	= 	( (fillVacant r' sz), s )
shiftRow r [] r' s	sz	= 	if not ((last r) == Blank) 	then (shiftRow (reverse (tail (reverse r))) ((last r) : []) ( (last r) : r' ) s sz) 
													else	(shiftRow (reverse (tail (reverse r))) [] r' s sz)
shiftRow r r_ r' s	sz	= 	if not ((last r) == Blank) 	
								then 	(	if (last r) == (head r_) 
												then 	(
															let (V val) = last r
															in ( shiftRow (reverse (tail (reverse r))) [] ( (V (2*val)) : (tail r') ) (s + (2* val) )  sz)
														)
												else	( shiftRow (reverse (tail (reverse r))) ((last r) : r_) ( (last r) : r' ) s sz)
										)
								else	( shiftRow (reverse (tail (reverse r))) r_ r' s sz) 

-- to fill the vacant spots created during shifting of rows
fillVacant :: [Cell Int] -> Int -> [Cell Int]
fillVacant row sz = row ++ [Blank | _ <- [1..(sz - (length row))] ]



-- we choose the argument to shiftBoard
applyMoves :: Board -> Int -> Char -> (Board, Int)
applyMoves board score move = 
	case move of
		'a' -> 
			let (b, s) = shiftBoard board
			in ( b , (score + s) )
		'w'	-> 
			let (b, s) = shiftBoard (transpose board)
			in ( (transpose b) , (score + s) )
		's' -> 
			let (b, s) = shiftBoard ( (transpose . reverse) board )
			in ( ( (reverse . transpose) b ) , (score + s) )
		'd' -> 
			let (b, s) = shiftBoard ( map reverse board ) 
			in ( (map reverse b) , (score + s) )
		'x' ->
			(board, score)

-- Random positions for two (only two yet)

emptyCells :: Board -> Int -> [(Int, Int)]
emptyCells [] _ 	= []
emptyCells (r:rs) row = [ (row, y) | (x, y) <- (zip r [s | s <- [1..] ]) , x == Blank ] ++ ( emptyCells rs (row + 1) )

updateBoard :: Board -> Int -> Int -> Int -> Board
updateBoard board x y p = ( take (x-1) board ) ++ [(updateRow (board !! (x-1)) y p)] ++ ( reverse ( take ((length board) - x) (reverse board) ) )

updateRow :: [Cell Int] -> Int -> Int -> [Cell Int]
updateRow row y p 	= ( take (y-1) row ) ++ [V p] ++ ( reverse ( take ((length row) - y) (reverse row) ) )


genToken :: Board -> Int -> Int -> IO Board
genToken board score typ = do
	g <- newStdGen
	let e = emptyCells board 1
	let n = length e
	-- this is the part where the logic will come
	let r = head $ ((randoms g) :: [Int])
	let (x, y) = e !! (r `mod` n )
	g1 <- newStdGen
	let k = head $ ((randoms g1) :: [Int])
	let z = chooseP (k `mod` 101)
	let ((a, b), (c, d)) = gameTree board 1 0
	-- putStr (show k) -- This is where I can insert AI
	if typ == 1 then return (updateBoard board c d b) else return (updateBoard board x y z)


chooseP :: Int -> Int
chooseP x = if x > 10	then 2 else 4

-- Board -> turn -> Count of call -> (Score, Number to be placed), (x, y)
gameTree :: Board -> Int -> Int -> ((Int, Int), (Int, Int))
gameTree board turn cnt		= if cnt == 5 then ((0, -1), (-1, -1))
												else
													if turn == 1 
														then
															-- call a minimise function on all the possible placements 
																let {
																	xs = emptyCells board 1;
																	b2 = map (\(e, f) -> updateBoard board e f 2) [pp | pp <- xs];
																 	b4 = map (\(e, f) -> updateBoard board e f 4) [pp | pp <- xs];
																 	res2 = map ( \b -> gameTree b (1-turn) (cnt+1)) b2;
																 	res4 = map ( \b -> gameTree b (1-turn) (cnt+1)) b4;
																 	a2 = [i | ((i, j), (k, l)) <- res2];
																 	a4 = [i | ((i, j), (k, l)) <- res4];
																 	r2 = if a2 == [] then 2000 else minimum a2;
																 	r4 = if a4 == [] then 2000 else minimum a4
																 	}

																in if r2 < r4 then (searchp r2 res2 xs 2) else (searchp r4 res4 xs 4)

														else
															-- call a maximise function on all the 4 moves
																let 
																	{
																	b = map (\c -> applyMoves board 0 c) "wasd";
																	res = map (\(bs, sc) -> gameTree bs (1-turn) (cnt+1)) b;
																	bres = zip b res;
																	as = [t + sc| ((bs, sc), ((t, _), (_, _))) <- bres];
																	r = if as == [] then 0 else maximum as
																	}
																in ((r, -1), (-1, -1))


searchp :: Int -> [((Int, Int), (Int, Int))] -> [(Int, Int)] -> Int -> ((Int,Int), (Int, Int))
searchp x [] [] num							= ((x, -1), (-1, -1))
searchp x (((i, j), (k, l)): xs) (y:ys) num= if i == x then do ((i, num), y) else searchp x xs ys num