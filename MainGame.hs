module Main
where
import Game
import UI
import GenToken
import System.IO
import Data.Char


main :: IO()
main = startGame

startGame :: IO()
startGame = do
	preMod <- hGetBuffering stdin
	hSetBuffering stdin NoBuffering
	putStr "\t \t START \n \n \n"
	putStr "\t Enter the size of the board you want ( 1 - 9 ): \n"
	sz <- (getInput "123456789")
	putStr "\t Easy - Press e \n\t Hard : Press h\n"
	typ <- (getInput "he")

	playGame (initBoard (digitToInt sz) ) 0 False (if typ == 'e' then 0 else 1)
	hSetBuffering stdin preMod