# CS 653: Introduction to Functional Programming #

## Project - 2048 : A Haskell implementation

```bash
$> make
$> bin/game
```
Press 'W','A','S','D' for movement.
Press 'X' to exit.

Also note that for board sizes >=5, the hard version will be slow as the min-max algorithm takes a lot of time. You have to be patient.

Shobhit Rastogi | 150690
Pranjal Prasoon | 150508

References: In the final report. 

