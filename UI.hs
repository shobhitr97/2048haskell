module UI
where

import Data.List

data Cell a = V a | Blank deriving (Eq, Show)

type Board = [[ Cell Int ]]


printCell :: String -> IO()
printCell s 		= do
	if s == (show "empty")
		then	putChar ' ' >> putStr (replicate 5 ' ') >> putChar '|'
		else	if s == (show "line")
						then	putStr (replicate 6 '-') >> putChar '+'--putChar '-' >> putChar '-' >> putChar '-' >> putChar '-' >> putChar '-' >> putChar '-' >> putChar '+'
						else	putChar ' ' >> putStr s >>	putStr (replicate (5 - (length s)) ' ') >> putChar '|'


printRow :: [String] -> IO()
printRow row = do
	putStr "\t\t|"
	mapM_ printCell row
	putStr "\n"
	putStr "\t\t+"
	mapM_ printCell (replicate (length row) (show "line") )
	putStr "\n"


prettyPrint :: Board -> Int -> IO()
prettyPrint board score = do
	putStr "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n\n\n"
	putStr "\t\t+"
	mapM_ printCell (replicate (length board) (show "line") )
	putStr "\n"
	mapM_ printRow (map (map cellToString) board)
	putStr "\n\n\n==============================================================\n\n"
	putStr "\tScore  : "
	putStr (show score)
	putStr "\n\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n"


cellToString :: Cell Int -> String
cellToString Blank = show "empty"
cellToString (V x) = show x